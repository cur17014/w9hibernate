import entity.Checkedout;
import entity.Inventory;
import entity.Patrons;
import entity.Phones;

import javax.persistence.*;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        // Entity manager
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        try {
            // Begin Transaction
            transaction.begin();

            seedDB(entityManager, transaction);

            // Check if Jack has a book checked out
            beginTrans(transaction);
            String sql = "SELECT p.Name, p.CardNum FROM library.patrons p INNER JOIN library.checkedout c INNER JOIN library.inventory i INNER JOIN Titles t WHERE p.Name = 'Aaron Curtis' AND p.CardNum = c.CardNum AND c.Serial = i.Serial AND i.ISBN = t.ISBN";
            Query doesPatronHaveCkedOut = entityManager.createNativeQuery(sql, Patrons.class);
            System.out.println("Books Checked Out By Me: " + doesPatronHaveCkedOut.getSingleResult());


            String patrons = "SELECT p.name, p.cardNum FROM Patrons p";
            List<Patrons> allPatrons = entityManager.createNamedQuery("Patrons.allEmp", Patrons.class).getResultList();
            System.out.println("Printing all employees \n");
            for(Patrons p : allPatrons){
                System.out.println(p);
            }
            beginTrans(transaction);
            transaction.commit();

        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }

    // Attempts to add some values to the DB
    public static void seedDB(EntityManager entityManager, EntityTransaction transaction) {

        String sql = "SELECT COUNT(*) FROM library.patrons";
        Query countPatrons = entityManager.createNativeQuery(sql);

        int count = Integer.parseInt(countPatrons.getSingleResult().toString());
        System.out.println("Number of patrons before seeding .." + countPatrons.getSingleResult());
        try {
            // if the count is 0, add a few items to the Database
            if (count > 0) {
                Query q = entityManager.createNativeQuery("SELECT COUNT(*) FROM library.patrons WHERE Name=:newName");

                Query phnExist = entityManager.createNativeQuery("SELECT COUNT(*) FROM library.phones WHERE Phone=:phone");

                for (int i = 0; i < 5; i++) {
                    beginTrans(transaction);

                    Patrons p = new Patrons();
                    boolean addedPatron = false;
                    int cardnm = i + 12;

                    String s = "SELECT COUNT(*) FROM library.patrons WHERE CardNum="+ cardnm;
                    q = entityManager.createNativeQuery(s);
                    int checkIfInDB = Integer.parseInt(q.getSingleResult().toString());

                    System.out.println("result of in db query: " + checkIfInDB);
                    if (!(checkIfInDB >= 1)) {
                        p.setName("Jack Sparrow" + i + "");
                        p.setCardNum(i + 12);
                        entityManager.persist(p);
                        addedPatron = true;
                        transaction.commit();
                    }


                    int phni = i + 11;
                    String phoneNumber = "30052" + phni;
                    phnExist.setParameter("phone", phoneNumber);
                    if(addedPatron) {
                        int checkPhnInDB = Integer.parseInt(phnExist.getSingleResult().toString());
                        if (!(checkPhnInDB >= 1)) {
                            beginTrans(transaction);
                            Phones phn = new Phones();
                            phn.setPhone(phoneNumber);
                            phn.setCardNum(i + 11);
                            entityManager.persist(phn);
                        }
                    }

                    transaction.commit();
                }
            }
        } catch(Exception e) {
            System.out.println("Error inserting into DB: " + e);
        }
    }

    private static void beginTrans(EntityTransaction transaction) {
        if(!transaction.isActive())
            transaction.begin();
    }
}

