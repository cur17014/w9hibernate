package entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Titles {
    private String isbn;
    private String title;
    private String author;
    private Collection<Inventory> inventoriesByIsbn;

    @Id
    @Column(name = "ISBN", nullable = false, length = 14)
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Basic
    @Column(name = "Title", nullable = false, length = 100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "Author", nullable = false, length = 100)
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Titles titles = (Titles) o;

        if (isbn != null ? !isbn.equals(titles.isbn) : titles.isbn != null) return false;
        if (title != null ? !title.equals(titles.title) : titles.title != null) return false;
        if (author != null ? !author.equals(titles.author) : titles.author != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = isbn != null ? isbn.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

/*
    @OneToMany(mappedBy = "titlesByIsbn")
    public Collection<Inventory> getInventoriesByIsbn() {
        return inventoriesByIsbn;
    }
*/

    public void setInventoriesByIsbn(Collection<Inventory> inventoriesByIsbn) {
        this.inventoriesByIsbn = inventoriesByIsbn;
    }

    @Override
    public String toString() {
        return "Titles{" +
                "isbn='" + isbn + '\'' +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
