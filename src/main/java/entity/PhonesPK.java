package entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class PhonesPK implements Serializable {
    private int cardNum;
    private String phone;

    @Column(name = "CardNum", nullable = false)
    @Id
    public int getCardNum() {
        return cardNum;
    }

    public void setCardNum(int cardNum) {
        this.cardNum = cardNum;
    }

    @Column(name = "Phone", nullable = false, length = 8)
    @Id
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhonesPK phonesPK = (PhonesPK) o;

        if (cardNum != phonesPK.cardNum) return false;
        if (phone != null ? !phone.equals(phonesPK.phone) : phonesPK.phone != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cardNum;
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        return result;
    }
}
