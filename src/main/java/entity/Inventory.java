package entity;

import javax.persistence.*;

@Entity
public class Inventory {
    private int serial;
    private String isbn;
    private Checkedout checkedoutBySerial;
    private Titles titlesByIsbn;

    @Id
    @Column(name = "Serial", nullable = false)
    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    @Basic
    @Column(name = "ISBN", nullable = false, length = 14)
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Inventory inventory = (Inventory) o;

        if (serial != inventory.serial) return false;
        if (isbn != null ? !isbn.equals(inventory.isbn) : inventory.isbn != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = serial;
        result = 31 * result + (isbn != null ? isbn.hashCode() : 0);
        return result;
    }

/*    @OneToOne(mappedBy = "inventoryBySerial")
    public Checkedout getCheckedoutBySerial() {
        return checkedoutBySerial;
    }*/

    public void setCheckedoutBySerial(Checkedout checkedoutBySerial) {
        this.checkedoutBySerial = checkedoutBySerial;
    }

/*    @ManyToOne
    @JoinColumn(name = "ISBN", referencedColumnName = "Titles.isbn", nullable = false)
    public Titles getTitlesByIsbn() {
        return titlesByIsbn;
    }*/

    public void setTitlesByIsbn(Titles titlesByIsbn) {
        this.titlesByIsbn = titlesByIsbn;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "serial=" + serial +
                ", isbn='" + isbn +
                '}';
    }
}
