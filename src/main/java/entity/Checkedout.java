package entity;

import javax.persistence.*;

@Entity
public class Checkedout {
    private int cardNum;
    private int serial;
    private Patrons patronsByCardNum;
    private Inventory inventoryBySerial;

    @Basic
    @Column(name = "CardNum", nullable = false)
    public int getCardNum() {
        return cardNum;
    }

    public void setCardNum(int cardNum) {
        this.cardNum = cardNum;
    }

    @Id
    @Column(name = "Serial", nullable = false)
    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Checkedout that = (Checkedout) o;

        if (cardNum != that.cardNum) return false;
        if (serial != that.serial) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cardNum;
        result = 31 * result + serial;
        return result;
    }

/*    @ManyToOne
    @JoinColumn(name = "CardNum", referencedColumnName = "CardNum", nullable = false)
    public Patrons getPatronsByCardNum() {
        return patronsByCardNum;
    }*/

/*    public void setPatronsByCardNum(Patrons patronsByCardNum) {
        this.patronsByCardNum = patronsByCardNum;
    }*/

/*    @OneToOne
    @JoinColumn(name = "Serial", referencedColumnName = "Serial", nullable = false)
    public Inventory getInventoryBySerial() {
        return inventoryBySerial;
    }*/

    public void setInventoryBySerial(Inventory inventoryBySerial) {
        this.inventoryBySerial = inventoryBySerial;
    }

    @Override
    public String toString() {
        return "Checkedout{" +
                "cardNum=" + cardNum +
                ", serial=" + serial +
                '}';
    }
}
