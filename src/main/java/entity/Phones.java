package entity;

import javax.persistence.*;

@Entity
@IdClass(PhonesPK.class)
public class Phones {
    private int cardNum;
    private String phone;
    /*    private Patrons patronsByCardNum;*/

    @Id
    @Column(name = "CardNum", nullable = false)
    public int getCardNum() {
        return cardNum;
    }

    public void setCardNum(int cardNum) {
        this.cardNum = cardNum;
    }

    @Id
    @Column(name = "Phone", nullable = false, length = 8)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phones phones = (Phones) o;

        if (cardNum != phones.cardNum) return false;
        if (phone != null ? !phone.equals(phones.phone) : phones.phone != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cardNum;
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Phones{" +
                "cardNum=" + cardNum +
                ", phone='" + phone + '\'' +
                '}';
    }

/*    @ManyToOne
    @JoinColumn(name = "CardNum", referencedColumnName = "CardNum", nullable = false)
    public Patrons getPatronsByCardNum() {
        return patronsByCardNum;
    }*/

/*    public void setPatronsByCardNum(Patrons patronsByCardNum) {
        this.patronsByCardNum = patronsByCardNum;
    }*/
}
