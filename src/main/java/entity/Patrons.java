package entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
@NamedQuery(name = "Patrons.allEmp", query = "SELECT p FROM Patrons p")
public class Patrons {
    private int cardNum;
    private String name;
    private Collection<Checkedout> checkedoutsByCardNum;
    private Collection<Phones> phonesByCardNum;

    @Id
    @Column(name = "CardNum", nullable = false)
    public int getCardNum() {
        return cardNum;
    }

    public void setCardNum(int cardNum) {
        this.cardNum = cardNum;
    }

    @Basic
    @Column(name = "Name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Patrons patrons = (Patrons) o;

        if (cardNum != patrons.cardNum) return false;
        if (name != null ? !name.equals(patrons.name) : patrons.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cardNum;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Patrons{" +
                "cardNum=" + cardNum +
                ", name='" + name + '\'' +
                '}';
    }
/*    @OneToMany(mappedBy = "patronsByCardNum")
    public Collection<Checkedout> getCheckedoutsByCardNum() {
        return checkedoutsByCardNum;
    }

    public void setCheckedoutsByCardNum(Collection<Checkedout> checkedoutsByCardNum) {
        this.checkedoutsByCardNum = checkedoutsByCardNum;
    }*/

/*
    @OneToMany(mappedBy = "patronsByCardNum")
    public Collection<Phones> getPhonesByCardNum() {
        return phonesByCardNum;
    }
*/

    public void setPhonesByCardNum(Collection<Phones> phonesByCardNum) {
        this.phonesByCardNum = phonesByCardNum;
    }
}
